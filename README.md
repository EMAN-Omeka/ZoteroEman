# Zotero Eman

Ce plugin permet de lister des éléments de bibliothèque Zotero sur des pages de contenu Omeka.

# Instructions

Voir le fichier [ZoteroEman-Manuel-Utilisateur.pdf](https://gitlab.com/EMAN-Omeka/ZoteroEman/-/blob/master/%20ZoteroEman-Manuel-Utilisateur.pdf)

# Credits

Développé par Vincent Buard (Numerizen)

Publié avec le soutien de l’École universitaire de recherche Translitteræ (programme Investissements d’avenir ANR‐10‐IDEX‐0001‐02 PSL* et ANR‐17‐EURE‐0025) 
