<?php

/*
 * Zotero Plugin
 *
 * Zotero data integration for Omeka Classic
 *
 */

class ZoteroEmanPlugin extends Omeka_Plugin_AbstractPlugin
{

  protected $_hooks = array(
    'initialize',
		'install',
		'uninstall',
		'define_acl',
		'define_routes',
  );

  protected $_filters = array(
  	'admin_navigation_main',
  );

  public function hookInitialize()
  {
      add_shortcode('zotero', [$this, 'shortcode']);
  }

  public function shortcode($args, $view)
  {
    return $this->displayInfo($args);
  }

  function hookDefineAcl($args)
  {
  	$acl = $args['acl'];
  	$ZoteroAdmin = new Zend_Acl_Resource('ZoteroEman_Page');
  	$acl->add($ZoteroAdmin);
  }

  /**
   * Add the pages to the public main navigation options.
   *
   * @param array Navigation array.
   * @return array Filtered navigation array.
   */
  public function filterAdminNavigationMain($nav)
  {
    $nav[] = array(
                    'label' => __('Zotero EMAN'),
                    'uri' => url('zotero'),
    								'resource' => 'ZoteroEman_Page',
                  );
    return $nav;
  }

  function hookDefineRoutes($args)
  {

  		$router = $args['router'];

  		$router->addRoute(
				'zotero_admin',
				new Zend_Controller_Router_Route(
						'zotero',
						array(
							'module' => 'zotero-eman',
							'controller'   => 'index',
							'action'       => 'admin',
						)
				)
  		);
  }

  public function displayInfo($entity) {

    $rootURL = get_option('zotero_root_url');
    $key = get_option('zotero_key');

    if (is_object($entity)) {
      $type = get_class($entity);
      $zoteroItems = metadata($entity, ['Zotero EMAN', 'URL List']);
      $zoteroTag = metadata($entity, ['Zotero EMAN', 'Library tag']);
      $zoteroSearch = metadata($entity, ['Zotero EMAN', 'Search phrase']);
    }
    if (is_array($entity)) {
      $zoteroItems = $entity['urls'];
      $zoteroTag =  $entity['tag'];
      $zoteroSearch =  $entity['search'];
    }

    if (! $zoteroItems && ! $zoteroTag && ! $zoteroSearch) { return '';}

    // Fetch Zotero data
    $curl = curl_init();
    if ($key ) {
      curl_setopt($curl, CURLOPT_HTTPHEADER, ['Zotero-API-Key: ' . $key]);
    }

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $zotero = '';
    $zoteroEntries = [];
    $treatedItems = [];
    if ($zoteroItems) {
      $treatedItems = [];
      $zoteroItems = explode(',', $zoteroItems);
      foreach ($zoteroItems as $i => $item) {
        $item = preg_replace("/\r|\n/", "", $item);
        if ($item) {
          curl_setopt($curl, CURLOPT_URL, $rootURL . '/items/' . $item);
          $zoteroQuery = curl_exec($curl);
          $zoteroData = json_decode($zoteroQuery, true);
          if ($zoteroData) {
            $zoteroEntries[] = $zoteroData['data'];
          }
          ! isset($bibName) ?  $bibName = $zoteroData[0]['library']['name'] : null;
          $treatedItems[] = $item;
        }
      }
    }

    if ($zoteroTag) {
      curl_setopt($curl, CURLOPT_URL, $rootURL . "/items?format=json&tag=$zoteroTag");
      $zoteroQuery = curl_exec($curl);
      $zoteroData = json_decode($zoteroQuery, true);
      foreach($zoteroData as $entry) {
        if (! in_array($entry['key'], $treatedItems)) {
          $treatedItems[] = $entry['key'];
          $zoteroEntries[] = $entry['data'];
          ! isset($bibName) ?  $bibName = $zoteroData[0]['library']['name'] : null;
        }
      }
    }

    if ($zoteroSearch) {
      $zoteroSearch = explode(',', $zoteroSearch);
      foreach ($zoteroSearch as $i => $search) {
        $search = urlencode($search);
        curl_setopt($curl, CURLOPT_URL, $rootURL . "/items?format=json&q=$search");
        $zoteroQuery = curl_exec($curl);
        $zoteroData = json_decode($zoteroQuery, true);
        foreach($zoteroData as $entry) {
          if (! in_array($entry['key'], $treatedItems)) {
            $treatedItems[] = $entry['key'];
              $zoteroEntries[] = $entry['data'];
            ! isset($bibName) ?  $bibName = $zoteroData[0]['library']['name'] : null;
          }
        }
      }
    }
    curl_close($curl);

    if (empty($zoteroEntries)) { return '';}

    // Titres
    $title = "Information Bibliographique";
    count($zoteroEntries) > 1 ? $title = "Informations Bibliographiques ($bibName)" : null;
    $content .= "<div class='field-uitemplates'><h2 style='width:90%;'>$title</h2><table style='width:90%;'><tr><th style='width:40%'>Titre</th><th style='width:20%'>Auteur</th><th style='width:10%'>Date</th><th style='width:30%'>Lien</th></tr>";
    $style = "";
    foreach ($zoteroEntries as $i => $entry) {
      $entry = ZoteroEmanPlugin::extractData($entry);
      $content .= "<tr $style>";
      $content .= "<td>" . $entry['title'] . '</td>';
      $content .= "<td>" . $entry['creators'] . '</td>';
      $content .= "<td>" . $entry['date'] . '</td>';
      $entry['publicationTitle'] <> '' ? $title = $entry['publicationTitle'] : $title = 'Lien externe';
      $entry['url'] <> '' ? $link = "<a style='width:800px;display:block;' target='_blank' href='" . $entry['url'] . "'>". $title . "</a>" : $link = '';
      $content .= "<td>$link</td>";
      $content .= '</tr>';
      $style == '' ? $style = "style='background-color:#ddd;'" : $style = '';
    }
    $content .= '</table></div>';
    return $content;
  }

  private function extractData($data) {
    $infos = [];
    $infos['title'] = $data['title'];
    $infos['creators'] = $data['creators'][0]['firstName'] . ' ' .  $data['creators'][0]['lastName'];
    $infos['date'] = $data['date'] ;
    $infos['url'] = $data['url'];
    $infos['publicationTitle'] = $data['publicationTitle'];
    return $infos;
  }

  /**
   * Install Zotero EMAN.
   */
  public function hookInstall()
  {
    $elementSetName = "Zotero EMAN";
  	// Don't install if an element set by the name "Zotero EMAN" already exists.
  	if (! $this->_db->getTable('ElementSet')->findByName($elementSetName)) {

    	$elementSetMetadata = ['name' => $elementSetName, 'description' => 'Zotero EMAN informations '];
    	$elements =[
    			['name' => 'URL List',	'description' => 'List of Zotero items\' Ids'],
    			['name' => 'Library tag',	'description' => 'Tag to use to fetch Zotero entries for this content.'],
    			['name' => 'Search phrase',	'description' => 'Phrase to search for in Title, Creator and Date entries.'],
    	];
    	insert_element_set($elementSetMetadata, $elements);
  	} else {
  		throw new Omeka_Plugin_Installer_Exception(
  				__('An element set by the name "%s" already exists. You must delete '
  						. 'that element set to install this plugin.', $elementSetName)
  		);
  	}
  }
  /**
   * Uninstall Zotero EMAN.
   */
  public function hookUninstall()
  {
    $elementSetName = "Zotero EMAN";
    // TODO : Delete Element Set on uninstall

/* Code to uninstall Vocabulary ?
    $builder = new Builder_ElementSet(get_db());
    Zend_Debug::dump(get_class_methods($builder));
*/
  }


}