<?php
class ZoteroEman_IndexController extends Omeka_Controller_AbstractActionController
{
  public function init() {
    $this->view->controller = $this;
    $this->db = get_db();
  }

  public function adminAction() {
    $form = $this->getForm();
		if ($this->_request->isPost()) {
			$formData = $this->_request->getPost();
			if ($form->isValid($formData)) {
  			set_option('zotero_root_url', $formData['rooturl']);
  			set_option('zotero_key', $formData['privatekey']);
      }
    }
    $this->view->content = $form;
  }

  private function getForm() {
    $form = new Zend_Form();

/*
		$rootURL = new Zend_Form_Element_Note('OptionsTitle');
		$optionsTitle->setValue("<strong>Le nombre de bloc est limité à 99, le nombre de champs à 9 et la longueur maximale à 9999.</strong><br/><br/>");
		$form->addElement($optionsTitle);
*/
    // Tags TODO : Add form element for tags
/*
    $rootURL = get_option('zotero_root_url');
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_URL, $rootURL .'/tags');
    $tags = curl_exec($curl);
    $tags = json_decode($tags, true);

    $content  = "<select name='zotero-tag' id='zotero-tags>'";
    foreach ($tags as $i => $tag) {
      $content .= "<option value='" . $tag['tag'] . "'>" . $tag['tag'] . "</option>";
    }
    $content .= '</select><br /><br />';
*/

		$rootURL = new Zend_Form_Element_Text('rooturl');
		$rootURL->setLabel('Libray root URL : ');
		$url = get_option('zotero_root_url');
		$rootURL->setValue($url);
		$rootURL->setAttrib('size', 100);
		$form->addElement($rootURL);

		$privateKey = new Zend_Form_Element_Text('privatekey');
		$privateKey->setLabel('Private Zotero access key : ');
		$key = get_option('zotero_key');
		$privateKey->setValue($key);
		$privateKey->setAttrib('size', 100);
		$form->addElement($privateKey);

    $submit = new Zend_Form_Element_Submit('submit');
    $submit->setLabel('Sauvegarder');
		$submit->setAttrib('class', 'add button small red');
		$submit->setAttrib('style', 'float:right;');
    $form->addElement($submit);

    return $this->prettifyForm($form);
  }

	private function prettifyForm($form) {
		$form->setDecorators(array(
				'FormElements',
				 array('HtmlTag', array('tag' => 'table')),
				'Form'
		));
		$form->setElementDecorators(array(
				'ViewHelper',
				'Errors',
				array(array('data' => 'HtmlTag'), array('tag' => 'td')),
				array('Label', array('tag' => 'td', 'class' => 'transcript-term-label', 'tagClass' => 'transcript-term-description-label')),
				array(array('row' => 'HtmlTag'), array('tag' => 'tr'))
		));
		return $form;
	}

}